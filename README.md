# Installation

1. Clone this repo
```bash
git clone https://gitlab.com/rattlehead/tomcat-notifier
cd tomcat-notifier
```

2. Build jar package
```bash
mvn clean package
```

3. Copy the package to your tomcat config folder. For Hybris:
```bash
cp target/tomcat-notifier* <HYBRIS_DIR>/config/customize/platform/tomcat/lib/
```

4. Add listener to the configuration. For Hybris:
```xml
# <HYBRIS_DIR>/config/tomcat/conf/server.xml
<Listener className="com.rattlehead.tomcatnotifier.NotifyListener" context="GenericJavaBeanResource"/>
```

5. Run ```ant customize``` and start the server.
