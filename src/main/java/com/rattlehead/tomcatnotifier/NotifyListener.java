package com.rattlehead.tomcatnotifier;

import org.apache.catalina.Lifecycle;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleListener;

import java.io.IOException;

public class NotifyListener implements LifecycleListener {

    private static final String EXEC = "notify-send";
    private static final String EXPIRE_TIME_SWITCH = "-t";
    private static final String DESKTOP_ENTRY_SWITCH = "-h";

    private static final int EXPIRE_TIME = 5000;
    private static final String DESKTOP_ENTRY = "string:desktop-entry:Hybris";
    private static final String TITLE = "Tomcat server";
    private static final String MESSAGE = "Hybris server has been started successfully";

    @Override
    public void lifecycleEvent(LifecycleEvent lifecycleEvent) {
        if (Lifecycle.AFTER_START_EVENT.equals(lifecycleEvent.getType())) {
            sendStartupNotification();
        }
    }

    private void sendStartupNotification() {
        try {
            String[] command = new String[]{EXEC, EXPIRE_TIME_SWITCH, String.valueOf(EXPIRE_TIME),
                    DESKTOP_ENTRY_SWITCH, DESKTOP_ENTRY, TITLE, MESSAGE};
            Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
